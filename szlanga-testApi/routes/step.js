var express = require('express');
var router = express.Router();

const pointToIndex = (point) => point[0] + point[1] * 16;

const getAction = ({ snake, apple, board }) => {
  const head = snake[0];
  if (
    head[1] > apple[1] &&
    board[pointToIndex([head[0], head[1] - 1])] != 'SNAKE'
  )
    return 'UP';

  if (
    head[1] < apple[1] &&
    board[pointToIndex([head[0], head[1] + 1])] != 'SNAKE'
  )
    return 'DOWN';

  if (
    head[0] > apple[0] &&
    board[pointToIndex([head[0] - 1, head[1]])] != 'SNAKE'
  )
    return 'LEFT';

  if (
    head[0] < apple[0] &&
    board[pointToIndex([head[0] + 1, head[1]])] != 'SNAKE'
  )
    return 'RIGHT';

  if (board[pointToIndex([head[0], head[1] - 1])] != 'SNAKE') return 'UP';

  if (board[pointToIndex([head[0], head[1] + 1])] != 'SNAKE') return 'DOWN';

  if (board[pointToIndex([head[0] - 1, head[1]])] != 'SNAKE') return 'LEFT';

  return 'RIGHT';
};

router.post('/', function (req, res, next) {
  const state = req.body;

  res.json({ action: getAction(state), state });
});

module.exports = router;
