import { Point, State, TileType, Direction } from './domain';

export const pointToIndex = (point: Point): number => point[0] + point[1] * 16;
export const indexToPoint = (index: number): Point => [
  index % 16,
  Math.floor(index / 16),
];
export const pointsEqual = (a: Point, b: Point) =>
  a[0] === b[0] && a[1] === b[1];

export const moveDirection = (direction: Direction): Point => {
  switch (direction) {
    case Direction.UP:
      return [0, -1];
    case Direction.DOWN:
      return [0, 1];
    case Direction.LEFT:
      return [-1, 0];
    case Direction.RIGHT:
      return [1, 0];
  }
};

export const boardFromState = ({ apple, snake }: State) =>
  Array.from({ length: 16 * 16 }, (_, index) => {
    const point: Point = indexToPoint(index);

    if (pointsEqual(point, apple)) return TileType.APPLE;
    if (pointsEqual(point, snake[0])) return TileType.HEAD;
    if (snake.some((snakePoint) => pointsEqual(point, snakePoint)))
      return TileType.SNAKE;
    return TileType.BLANK;
  });

export const getAvailableTiles = ({ snake }: State): Point[] =>
  Array.from({ length: 16 * 16 }, (_, k) => k)
    .map(indexToPoint)
    .filter((tile) => !snake.some((snakeTile) => pointsEqual(tile, snakeTile)));
