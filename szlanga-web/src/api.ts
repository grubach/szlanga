import { State } from './domain';
import { apiURL } from './constants';

export const call = (state: State) =>
  fetch(apiURL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(state),
  });
