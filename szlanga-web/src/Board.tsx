import React from 'react';
import { State } from './domain';
import Tile from './Tile';
import './Board.css';

type Props = {
  state: State;
};

const Board: React.FC<Props> = ({ state: { board, gameOver } }) => (
  <div className={`Board ${gameOver ? 'Board--game-over' : ''}`}>
    {board.map((tile, index) => (
      <Tile key={index} tile={tile} />
    ))}
  </div>
);

export default Board;
