import React from 'react';
import { State } from './domain';
import './Status.css';

type Props = {
  state: State;
};

const Status: React.FC<Props> = ({ state: { score, step } }) => {
  return (
    <div className={`Status`}>
      <div className='Status__field'>{step}</div>
      <div className='Status__field'>{score}</div>
    </div>
  );
};

export default Status;
