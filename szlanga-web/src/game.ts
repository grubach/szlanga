import { State, Point, Direction } from './domain';
import {
  getAvailableTiles,
  pointsEqual,
  boardFromState,
  moveDirection,
} from './utils';
import { call } from './api';
import { interval, stabilized } from './constants';

const initialState: State = {
  snake: [
    [8, 8],
    [8, 9],
    [8, 10],
  ],
  apple: [8, 4],
  step: 0,
  score: 0,
  gameOver: false,
  board: [],
};

const getInitialState = (): State => ({
  ...initialState,
  board: boardFromState(initialState),
});

const placeApple = (state: State): Point => {
  const { apple } = state;
  const blankTiles: Point[] = getAvailableTiles(state).filter(
    (tile) => !pointsEqual(tile, apple)
  );

  const randomTileIndex = Math.floor(Math.random() * blankTiles.length);
  return blankTiles[randomTileIndex];
};

const check = (state: State, direction: Direction): State => {
  const { snake, apple, score, step } = state;
  const head = snake[0];

  const move = moveDirection(direction);
  const nextHead: Point = [head[0] + move[0], head[1] + move[1]];

  if (pointsEqual(nextHead, apple)) {
    return {
      ...state,
      snake: [...snake, [0, 0]],
      apple: placeApple(state),
      score: score + 1,
      step: step + 1,
    };
  }

  if (
    !getAvailableTiles(state).some((availableTile) =>
      pointsEqual(nextHead, availableTile)
    )
  ) {
    return {
      ...state,
      gameOver: true,
      step: step + 1,
    };
  }

  return { ...state, step: step + 1 };
};

const proceed = (state: State, direction: Direction): State => {
  const { snake, gameOver } = state;

  if (gameOver) return state;

  const tail = snake.slice(0, -1);
  const oldHead = tail[0];

  const move = moveDirection(direction);
  const head: Point = [oldHead[0] + move[0], oldHead[1] + move[1]];

  const nextSnake: Point[] = [head, ...tail];

  return {
    ...state,
    snake: nextSnake,
  };
};

const updateBoard = (state: State): State => ({
  ...state,
  board: boardFromState(state),
});

const update = (state: State, direction: Direction): State => {
  if (state.gameOver) return getInitialState();

  const checkedState = check(state, direction);
  const proceededState = proceed(checkedState, direction);
  const newState = updateBoard(proceededState);
  return newState;
};

export let gameState = getInitialState();

const loop = () => {
  const apiCall = () =>
    call(gameState)
      .then((response) => response.json())
      .then(({ action }) => {
        gameState = update(gameState, action);
        loop();
      });

  if (!stabilized) {
    apiCall();
    return;
  }

  setTimeout(() => {
    apiCall();
  }, interval);
};

loop();
