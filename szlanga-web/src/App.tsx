import React from 'react';
import './App.css';
import Board from './Board';
import Status from './Status';
import { gameState } from './game';

function App() {
  const [state, setState] = React.useState(gameState);

  const update = React.useCallback(() => {
    setState(gameState);
    requestAnimationFrame(update);
  }, []);

  React.useEffect(() => {
    update();
  }, [update]);

  return (
    <div className='App'>
      <Status state={state} />
      <Board state={state} />
    </div>
  );
}

export default App;
