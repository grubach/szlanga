import React from 'react';
import { TileType } from './domain';
import './Tile.css';
type Props = {
  tile: TileType;
};

const Tile: React.FC<Props> = ({ tile }) => (
  <div className={`Tile Tile--${tile}`}></div>
);

export default Tile;
