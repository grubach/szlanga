export enum TileType {
  BLANK = 'BLANK',
  SNAKE = 'SNAKE',
  HEAD = 'HEAD',
  APPLE = 'APPLE',
}

export enum Direction {
  UP = 'UP',
  DOWN = 'DOWN',
  RIGHT = 'RIGHT',
  LEFT = 'LEFT',
}

export enum Action {
  TURN_LEFT = 'TURN_LEFT',
  TURN_RIGHT = 'TURN_RIGHT',
  FORWARD = 'FORWARD',
}

export type Point = [number, number];

export type State = {
  board: TileType[];
  snake: Point[];
  apple: Point;
  step: number;
  score: number;
  gameOver: boolean;
};
